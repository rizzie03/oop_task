// Temen-temen silakan buat sebuah object representasikan pakek class
// 1. Buat class parent dalam nya ada instance & static property & method
// 2. Buat class child dari inheritance class parent. Class childnya juga punya instance & static property dan methodnya sendiri
// 3. Instansiasi ke dua class tersebut jalankan semua method yang ada, sama console semua property yang ada

class Actor {
  static isWorkinginFilmIndustry = true;
  static group = "Film Indusrty";
  constructor(name, genre) {
    this.name = name;
    this.genre = genre;
  }
  introduce() {
    console.log(`This actor is called ${this.name}`);
  }
  work() {
    console.log("Work!");
  }
}

class Movies extends Actor {
  constructor(name, genre, films) {
    super(name, genre);
    this.films = films;
  }
  introduce() {
    super.introduce();
    console.log(`The lead actor for ${this.films} is ${this.name}`);
  }
  film() {
    console.log(
      "His films are ",
      this.films[Math.floor(Math.random() * this.films.length)]
    );
  }
}

let Pirates = new Movies(
  "Pirates of the Caribbean Series",
  "Action, Adventure"
);
Pirates.introduce();

let Johnny = new Movies(
  "Pirates of the Caribbean Series",
  "Action, Adventure",
  [
    "Pirates of the Caribbean: The Curse of the Black Pearl ",
    "Pirates of the Caribbean: Dead Man's Chest ",
    "Pirates of the Caribbean: At World's End ",
    "Pirates of the Caribbean: On Stranger Tides ",
    "Pirates of the Caribbean: Dead Men Tell No Tales ",
  ]
);
Johnny.introduce();
Johnny.film();
Johnny.work();

try {
  Johnny.film();
} catch (err) {
  console.log(err.message);
}

console.log(Johnny instanceof Actor);
console.log(Johnny instanceof Movies);
console.log(Actor.isWorkinginFilmIndustry);
console.log(Actor.group);
